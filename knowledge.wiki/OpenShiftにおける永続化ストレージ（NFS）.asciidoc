= OpenShiftにおける永続化ストレージ（NFS）

https://docs.okd.io/3.9/install_config/persistent_storage/persistent_storage_nfs.html

== 作成

.nfs-pv.yaml
----
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0001
spec:
  capacity:
    storage: 1Gi 
  accessModes:
  - ReadWriteOnce 
  nfs: 
    path: /exports/data
    server: opt.okd.test
  persistentVolumeReclaimPolicy: Recycle
----

----
oc create -f nfs-pv.yaml
oc get pv

oc delete pv pv0001
----

.nfs-claim.yaml
----
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: nfs-claim1
spec:
  accessModes:
    - ReadWriteOnce 
  resources:
    requests:
      storage: 100Mi
----

----
oc create -f nfs-claim.yaml
----

* 割り当てる領域がないとpendingのまま
* 容量を小さくしても、１つのPV全体で割り当てられる？
* claimを使うときはサービス側でサブディレクトリ指定するなどで複数サービスで使い回すことも可能。前もってディレクトリを作成しておくこと。

