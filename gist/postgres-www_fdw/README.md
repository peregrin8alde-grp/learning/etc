# Postgresの外部テーブルでREST

## install
- make時にエラーが発生したため保留
    - postgresのバージョンが11だったため、未サポートの可能性もあり

```
sudo apt update 
&& apt install -y \
     gcc make \
     git \
     postgresql-server-dev-$PG_MAJOR \
     libcurl4-openssl-dev libjson-c-dev libxml2-dev 

su postgres
git clone https://github.com/cyga/www_fdw.git
cd www_fdw
export USE_PGXS=1
make && make install
```
