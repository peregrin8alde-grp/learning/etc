# Grav

https://getgrav.org/

## docker
```
mkdir -p ~/grav
mkdir -p ~/php/config
mkdir -p ~/php/html

curl -L -o ~/grav/grav-admin-1.4.5.zip https://getgrav.org/download/core/grav-admin/1.4.5
cd ~/grav
unzip grav-admin-1.4.5.zip
cp -rpf grav-admin ~/php/html/grav
chmod -R a+w ~/php/html/grav

echo "extension=gd.so" > ~/php/config/php.ini
echo "extension=zip.so" >> ~/php/config/php.ini

echo '<?php phpinfo();' > ~/php/html/index.php

docker run -d \
  --name php7 \
  -p 10080:80 \
  -v ~/php/config:/usr/local/etc/php \
  -v ~/php/html:/var/www/html \
  php:7-apache
```

```
docker exec -it php7 /bin/bash

apt-get update && apt-get install -y \
        libjpeg62-turbo-dev \
        libpng-dev \
        zlib1g-dev \
    && docker-php-ext-configure gd --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install zip
a2enmod rewrite

exit

docker restart php7
```

http://0.0.0.0:10080/grav/
パスワードは大文字小文字を混ぜる必要がある。

- 管理画面は`/admin`
    - プラグインの`Admin Panel`の設定で変更可能
    - デフォルトのホームやプラグインの説明にある`/user`はここのことっぽい。
        - 正確には、実際のマシン上のディレクトリ名が`gravインストール先/user`
- 色々テーマやプラグインをインストールしないと使いにくい。
    - テーマ：lear2やknowledgeが良さそう。
        - テーマ変更したらホーム画面（上記の`/grav`直下）で確認
        - 各テーマの設定やディレクトリ構造、ファイル名（記事種別）に注意
    - プラグイン：
        - ユーザ管理系はあったほうが良い。一応、`/admin/user/ユーザ名`で設定画面にいける。
            - loginプラグインの設定で色々できるので、それで十分かも
- フォルダ名に数字プレフィックスをつけてソート可能にする。
