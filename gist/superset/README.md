# Apache Superset

https://superset.incubator.apache.org/installation.html

## docker
```
git clone https://github.com/apache/incubator-superset/
cd incubator-superset
cp contrib/docker/{docker-build.sh,docker-compose.yml,docker-entrypoint.sh,docker-init.sh,Dockerfile} .
cp contrib/docker/superset_config.py superset/
bash -x docker-build.sh
```

- 通常はdocker-compose。下記はcompose使えない場合の想定

```
docker run -d \
  --name superset_postgres \
  -v superset_postgres:/var/lib/postgresql/data \
  -e POSTGRES_DB=superset \
  -e POSTGRES_USER=superset \
  -e POSTGRES_PASSWORD=superset \
  postgres
docker run -d \
  --name superset_redis \
  -v redis:/data \
  redis

docker run -d \
  --name superset \
  -p 8088:8088 \
  -v ~/incubator-superset:/home/work/incubator-superset \
  -v superset-node-modules:/home/work/incubator-superset/superset/assets/node_modules \
  --link superset_postgres:postgres \
  --link superset_redis:redis \
  -e POSTGRES_DB=superset \
  -e POSTGRES_USER=superset \
  -e POSTGRES_PASSWORD=superset \
  -e POSTGRES_HOST=postgres \
  -e POSTGRES_PORT=5432 \
  -e REDIS_HOST=redis \
  -e REDIS_PORT=6379 \
  -e SUPERSET_ENV=local \
  apache/incubator-superset \
    tail -f /dev/null

docker exec -it superset /bin/bash

bash docker-init.sh
```

