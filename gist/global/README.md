# GNU Global

## install
Dockerfile参照

```
docker build \
  -t my/global:httpd \
  -f Dockerfile_httpd .
```

## 検索CGI
- cgiの中でglobalの絶対パスを指定しているので、別環境でhtagsを実行した場合は注意
- gtagsの結果ファイルも参照するので、まとめて配置

```
docker run -d \
  --name htags-sv \
  -p 10080:80 \
  -v /usr/local/apache2/htdocs/ \
  my/global:httpd
  
docker run -it \
  --rm \
  --volumes-from htags-sv \
  -v $(pwd):/src \
  -w /src \
  my/global:httpd \
    /bin/bash
```

```
gtags
htags --suggest2
mkdir -p /usr/local/apache2/htdocs/work1
mv GPATH GRTAGS GTAGS HTML /usr/local/apache2/htdocs/work1/
exit
```

http://0.0.0.0:10080/work1/HTML/
