# Pico検索プラグイン

https://github.com/PontusHorn/Pico-Search

- `isLowValueWord`がバグってるように見える。
   - メソッドの引数である`word`と`in_array`の引数に指定してる`searchTerm`の変数名不整合
   - `low_value_words`が定義されてない場合の挙動
- Readmeが古そう。
    - 検索フォームのサンプルで`location.href = '/search/'`とあるが、これでは検索用ファイルにアクセスできない。
        - `"{{ "index"|link }}" + '?search/'`
        - サブフォルダに入れた場合は`"{{ "sub/index"|link }}" + '/search/'`
    - `low_value_words`の意味や指定方法が書かれてない。
      ```
      ##
      # PicoSearch
      #
      search_excludes: []
      PicoSearch:
          low_value_words: []
      ```
- pagingはプラグイン使うよりform_param/url_param+twig+javascriptのほうが自由がききそう。
    - URL指定は`?記事ID`部分から通常の意味ではクエリなためパラメタ追加時には注意
