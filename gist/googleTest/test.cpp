#include "gtest/gtest.h"

extern "C" {
  #include "tmp.h"
}

TEST(TestCase1, TestName1) {
  EXPECT_EQ(func(1, 2), 3);
}

TEST(TestCase1, TestName2) {
  EXPECT_EQ(func2("test"), 0);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}