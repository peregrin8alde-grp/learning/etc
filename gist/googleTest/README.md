# Google Test

## install
```
git clone https://github.com/google/googletest.git
cd googletest
export GTEST_DIR=$(pwd)/googletest
```

```
g++ -isystem ${GTEST_DIR}/include -I${GTEST_DIR} \
    -pthread -c ${GTEST_DIR}/src/gtest-all.cc
ar -rv libgtest.a gtest-all.o
```

- cmake利用時
```
mkdir mybuild       # Create a directory to hold the build output.
cd mybuild
cmake ${GTEST_DIR}  # Generate native build scripts.
```

## test
```
g++ -isystem ${GTEST_DIR}/include -pthread path/to/your_test.cc libgtest.a \
    -o your_test
```

```
cd ${GTEST_DIR}/make
make
./sample1_unittest
```

## try
- targetはあくまでライブラリなのでmainを含んではいけない。
- targetのライブラリをリンクしてヘッダをインクルード（cの場合は`extern "C"`で囲む）
- main用のライブラリをリンクしたらmainを自分で書かなくて良いらしい。
    - テスト対象を選ぶなど改造したい場合は自分で書く？
- googletestは使用するライブラリ（`libgtest.a`）とヘッダ（`include`配下）を参照できればいい。
```
gcc -c -I./include src/tmp.c

g++ -isystem ./test/include \
  -pthread \
  -I./include \
  ./test/src/test.cpp \
  ./test/lib/libgtest.a \
  ./tmp.o \
  -o ./test/test
```

```
$ ./test/test
[==========] Running 2 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 2 tests from TestCase1
[ RUN      ] TestCase1.TestName1
string : test
./test/src/test.cpp:8: Failure
Expected equality of these values:
  func(1, 2)
    Which is: 3
  2
[  FAILED  ] TestCase1.TestName1 (0 ms)
[ RUN      ] TestCase1.TestName2
string : test
[       OK ] TestCase1.TestName2 (0 ms)
[----------] 2 tests from TestCase1 (1 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test case ran. (1 ms total)
[  PASSED  ] 1 test.
[  FAILED  ] 1 test, listed below:
[  FAILED  ] TestCase1.TestName1

 1 FAILED TEST
```

```
$ ./test/test --help
This program contains tests written using Google Test. You can use the
following command line flags to control its behavior:

Test Selection:
  --gtest_list_tests
      List the names of all tests instead of running them. The name of
      TEST(Foo, Bar) is "Foo.Bar".
  --gtest_filter=POSTIVE_PATTERNS[-NEGATIVE_PATTERNS]
      Run only the tests whose name matches one of the positive patterns but
      none of the negative patterns. '?' matches any single character; '*'
      matches any substring; ':' separates two patterns.
  --gtest_also_run_disabled_tests
      Run all disabled tests too.

Test Execution:
  --gtest_repeat=[COUNT]
      Run the tests repeatedly; use a negative count to repeat forever.
  --gtest_shuffle
      Randomize tests' orders on every iteration.
  --gtest_random_seed=[NUMBER]
      Random number seed to use for shuffling test orders (between 1 and
      99999, or 0 to use a seed based on the current time).

Test Output:
  --gtest_color=(yes|no|auto)
      Enable/disable colored output. The default is auto.
  --gtest_print_time=0
      Don't print the elapsed time of each test.
  --gtest_output=(json|xml)[:DIRECTORY_PATH/|:FILE_PATH]
      Generate a JSON or XML report in the given directory or with the given
      file name. FILE_PATH defaults to test_detail.xml.
  --gtest_stream_result_to=HOST:PORT
      Stream test results to the given server.

Assertion Behavior:
  --gtest_death_test_style=(fast|threadsafe)
      Set the default death test style.
  --gtest_break_on_failure
      Turn assertion failures into debugger break-points.
  --gtest_throw_on_failure
      Turn assertion failures into C++ exceptions for use by an external
      test framework.
  --gtest_catch_exceptions=0
      Do not report exceptions as test failures. Instead, allow them
      to crash the program or throw a pop-up (on Windows).

Except for --gtest_list_tests, you can alternatively set the corresponding
environment variable of a flag (all letters in upper-case). For example, to
disable colored text output, you can either specify --gtest_color=no or set
the GTEST_COLOR environment variable to no.

For more information, please read the Google Test documentation at
https://github.com/google/googletest/. If you find a bug in Google Test
(not one in your own code or tests), please report it to
<googletestframework@googlegroups.com>.
```