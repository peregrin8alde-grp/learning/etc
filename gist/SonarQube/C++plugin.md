# C/C++用プラグイン

コミュニティ版でも使用可能なもの

https://github.com/SonarOpenCommunity/sonar-cxx

## install
- 実施後にSonarQubeサーバ再起動
```
sudo curl -Lo ~/sonarqube/extensions/plugins/sonar-c-plugin-1.1.0.jar \
  https://github.com/SonarOpenCommunity/sonar-cxx/releases/download/cxx-1.1.0/sonar-c-plugin-1.1.0.jar
sudo curl -Lo ~/sonarqube/extensions/plugins/sonar-cxx-plugin-1.1.0.jar \
  https://github.com/SonarOpenCommunity/sonar-cxx/releases/download/cxx-1.1.0/sonar-cxx-plugin-1.1.0.jar
```

