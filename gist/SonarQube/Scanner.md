# SonarQube Scanner

https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner

## docker image
- JRE同梱版だとunzipでsymbolic linkに関するエラーが発生したため、単体版をopenjdk上にインストール

```
FROM openjdk:alpine

RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227.zip \
    && unzip sonar-scanner-cli-3.2.0.1227.zip \
    && rm sonar-scanner-cli-3.2.0.1227.zip \
    && mv sonar-scanner-3.2.0.1227 /sonar-scanner

WORKDIR /src

ENTRYPOINT ["/sonar-scanner/bin/sonar-scanner"]
```

## プロパティファイルを使った実行

- `sonar-project.properties`をルートに格納
- サーバとの通信がhttpsの場合、javaのssl設定に注意。
    - 自己証明書の場合、javaオプションで信頼させる必要がある？
      ```
      -Djavax.net.ssl.trustStore=（任意のディレクトリ）/server/cacerts \
      -Djavax.net.ssl.keyStore=（任意のディレクトリ）/server/'server.p12' \
      -Djavax.net.ssl.keyStorePassword='serverpassword'
      ```

```
# Serverを個別に指定する場合
sonar.host.url=http://sonarqube:9000

# must be unique in a given SonarQube instance
sonar.projectKey=my:project
# this is the name and version displayed in the SonarQube UI. Was mandatory prior to SonarQube 6.1.
sonar.projectName=My project
sonar.projectVersion=1.0

sonar.language=c++

# Path is relative to the sonar-project.properties file. Replace "\" by "/" on Windows.
# This property is optional if sonar.modules is set. 
sonar.sources=.
 
# Encoding of the source code. Default is default system encoding
#sonar.sourceEncoding=UTF-8
```
