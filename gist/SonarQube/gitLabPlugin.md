# GitLab連携プラグイン

https://github.com/gabrie-allaigre/sonar-gitlab-plugin/

## install
- 実施後にSonarQubeサーバ再起動
```
sudo curl -Lo ~/sonarqube/extensions/plugins/sonar-gitlab-plugin-4.0.0.jar \
  https://github.com/gabrie-allaigre/sonar-gitlab-plugin/releases/download/4.0.0/sonar-gitlab-plugin-4.0.0.jar
```

## 利用
- Administration - Configuration - Gitlab
- Scanner
  `-Dsonar.login=$SONAR_LOGIN`はログイン情報を渡す場合に指定。  
  ```
  sonar-scanner \
      -Dsonar.host.url=$SONAR_URL \
      -Dsonar.login=$SONAR_LOGIN \
      -Dsonar.analysis.mode=preview \
      -Dsonar.gitlab.commit_sha=$CI_COMMIT_SHA \
      -Dsonar.gitlab.ref_name=$CI_COMMIT_REF_NAME \
      -Dsonar.gitlab.project_id=$CI_PROJECT_ID
  ```
- externalジョブが実行され、コミットのchangesやマージリクエストのDiscussionにコメントされる。