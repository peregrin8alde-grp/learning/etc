# Code checkers

https://github.com/SonarOpenCommunity/sonar-cxx/wiki/Code-checkers

## cppcheck
```
FROM alpine

RUN apk update \
    && apk add cppcheck \
    && rm -rf /var/cache/apk/*

WORKDIR /src

ENTRYPOINT ["cppcheck"]
```

## GitLab CIでの利用例
- Cppcheck report(s)の設定で`report.xml`を指定

```
cppcheck:
  stage: build
  tags:
    - tag1
  image:
    name: my/cppcheck
    # alpineイメージベースだとENTRYPOINTを上書きして空文字にしないとエラーとなる？
    entrypoint: [""]
  script: cppcheck -v --enable=all --xml -Iinclude src 2> report.xml
  artifacts:
    paths:
    - report.xml

sonar-scanner:
  stage: test
  tags:
    - tag1
  image:
    name: my/sonarscanner:3.2
    # alpineイメージベースだとENTRYPOINTを上書きして空文字にしないとエラーとなる？
    entrypoint: [""]
  script: /sonar-scanner/bin/sonar-scanner
  dependencies:
    - cppcheck
```
