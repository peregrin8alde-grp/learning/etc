# SonarQube

https://www.sonarqube.org/
https://docs.sonarqube.org/

## install
```
mkdir -p ~/sonarqube/{logs,data,extensions/plugins}
docker run \
  -d \
  --name sonarqube \
  -p 19000:9000 \
  -p 19092:9092 \
  -v ~/sonarqube/logs:/opt/sonarqube/logs \
  -v ~/sonarqube/data:/opt/sonarqube/data \
  -v ~/sonarqube/extensions/plugins:/opt/sonarqube/extensions/plugins \
  sonarqube
```

admin/admin
