# Web API
https://service-knowledge.support-project.org/open.knowledge/view/19

- Access tokenを使ってアクセス可能。
- 現状はGET系のみ？
    - コメントで追記されている投稿／更新／削除も可能
- 一覧取得で取得する情報が詳細取得と同じ？
    - 記事内容まで取得されるように見える。
    - しかも10件までしか取得されない？
        - `offset`と`limit`パラメタがある。`limit`は最大50
          https://github.com/support-project/knowledge/blob/8423ec9d7ce62ba932849a538aae89954c9a6e57/src/main/java/org/support/project/web/control/GetApiControl.java

- パラメータ指定
```
curl -X GET http://[Knowledgeホスト]/api/knowledges?private_token=[TOKEN]
curl -X GET http://[Knowledgeホスト]/api/knowledges/[ID]?private_token=[TOKEN]
```

- ヘッダ指定（推奨？）
```
curl -X GET -H 'PRIVATE-TOKEN: [TOKEN]' http://[Knowledgeホスト]/api/knowledges/[ID]
curl -X DELETE -H 'PRIVATE-TOKEN: [TOKEN]' http://[Knowledgeホスト]/api/knowledges/[ID]
curl -X POST -H 'PRIVATE-TOKEN: [TOKEN]' -H 'Content-Type:application/json' \
     -d '{"content":"# api","publicFlag":1,"tags":["api"], "template":"knowledge", "title":"try api" }' \
     http://[Knowledgeホスト]/api/knowledges
curl -X POST -H 'PRIVATE-TOKEN: [TOKEN]' -H 'Content-Type:application/json' \
     -d @test.json \
     http://[Knowledgeホスト]/api/knowledges
```

- 記事例
```
{"attachments":[],"commentCount":0,"comments":[],"content":"# try","editors":{"groups":[],"users":[]},"insertDatetime":"2018-06-08 12:34:40.268","insertUser":1,"knowledgeId":1,"likeCount":0,"publicFlag":1,"tags":[""],"template":"knowledge","templateItems":[],"title":"try","updateDatetime":"2018-06-08 12:34:40.268","updateUser":1,"viewers":{"groups":[],"users":[]}}h
```