# webhook
- Test Hookではサンプルメッセージが送られるせいで相手次第で形式エラーが発生する。
- 通常の投稿で発生しても、非同期で実行される定期（おそらく3分毎）バッチで送信されるため、しばらく待たないと送信されない？

## Mattermostへの送信例
https://service-knowledge.support-project.org/open.knowledge/view/72

- ナレッジ
```
{
    "channel": "knowledge",
    "username": "knowledge",
    "icon_url": "http://<knowledgeをホストしているサーバ>/knowledge/favicon.ico",
    "text": "<{knowledge.link}|{knowledge.title}>",
    "attachments": [
     {
      "fallback": "<{knowledge.link}|{knowledge.title}>",
      "title": "{knowledge.title}",
      "title_link": "{knowledge.link}",
      "color": "#808080",
      "fields": [
       {
        "title": "作成日時",
        "value": "{knowledge.insertDatetime,format=yyyy/MM/dd HH:mm:ss.SSSZ}",
        "short": true
       },
       {
        "title": "作成者",
        "value": "{knowledge.insertUserName}",
        "short": true
       },
       {
        "title": "更新日時",
        "value": "{knowledge.updateDatetime,format=yyyy/MM/dd HH:mm:ss.SSSZ}",
        "short": true
       },
       {
        "title": "更新者",
        "value": "{knowledge.updateUserName}",
        "short": true
       },
       {
        "title": "投稿グループ",
        "value": "{knowledge.groups}",
        "short": true
       },
       {
        "title": "タグ",
        "value": "{knowledge.tags}",
        "short": true
       },
       {
        "title": "コンテンツ",
        "value": "{knowledge.content}",
        "short": true
       }
      ]
     }
    ]
}
```

- コメント
```
{
    "channel": "knowledge",
    "username": "knowledge",
    "icon_url": "http://<knowledgeをホストしているサーバ>/knowledge/favicon.ico",
    "text": "<{knowledge.link}|{knowledge.title}>へのコメント",
    "attachments": [
     {
      "fallback": "<{knowledge.link}|{knowledge.title}>へのコメント",
      "title": "{knowledge.title}へのコメント",
      "title_link": "{knowledge.link}",
      "color": "#808080",
      "fields": [
       {
        "title": "コメント書込日時",
        "value": "{comment.insertDatetime,format=yyyy/MM/dd HH:mm:ss.SSSZ}",
        "short": true
       },
       {
        "title": "コメント書込者",
        "value": "{comment.insertUserName}",
        "short": true
       },
       {
        "title": "コメント更新日時",
        "value": "{comment.updateDatetime,format=yyyy/MM/dd HH:mm:ss.SSSZ}",
        "short": true
       },
       {
        "title": "コメント更新者",
        "value": "{comment.updateUserName}",
        "short": true
       },
       {
        "title": "コメントNo",
        "value": "{comment.commentNo}",
        "short": true
       },
       {
        "title": "投稿グループ",
        "value": "{knowledge.groups}",
        "short": true
       },
       {
        "title": "コンテンツ",
        "value": "{comment.comment}",
        "short": true
       }
      ]
     }
    ]
}
```

