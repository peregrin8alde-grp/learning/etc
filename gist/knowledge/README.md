# Knowledge
- https://service-knowledge.support-project.org/open.knowledge/list
- https://information-knowledge.support-project.org/ja/
- https://github.com/support-project/knowledge/releases
- https://information-knowledge.support-project.org/ja/manual
- https://github.com/support-project/docker-knowledge

## docker
```
docker pull koda/docker-knowledge
mkdir /home/hoge/knowledge
chmod a+w /home/hoge/knowledge
docker run -d -p 80:8080 -v /home/hoge/knowledge:/root/.knowledge --name knowledge koda/docker-knowledge
```
