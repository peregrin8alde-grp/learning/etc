# Pico編集プラグイン

https://github.com/blocknotes/pico_edit

古いのが気になるところ

```
##
# pico_edit
#
pico_edit_404: false
pico_edit_options: false  # Disallow options editing
pico_edit_default_author: 'Me'  # Default author for new pages
```

- Picoが動作するユーザに対してconfigやcontenなどへの書き込み権限を与える必要がある。
- `pico_edit.md`をルート直下に用意しておくとデフォルトテーマならヘッダにリンクができて便利
    - できれば現在開いてるページから編集画面に移りたいところ
    