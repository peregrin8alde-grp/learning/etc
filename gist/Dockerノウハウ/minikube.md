# minikube
ローカルマシン上にシングルノードのクラスタを構築できる。
VMドライバを作った場合はVM上にdockerが使えるマネージャ兼ノードができるが、
リソースが限定されるので注意。

## install
### Install kubectl
https://kubernetes.io/docs/tasks/tools/install-kubectl/

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

### minikube
https://github.com/kubernetes/minikube/releases

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.25.0/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
```

https://github.com/kubernetes/minikube/blob/v0.25.0/README.md

- vmドライバはKVM2を使う。
  https://github.com/kubernetes/minikube/blob/master/docs/drivers.md#kvm2-driver
    - Linuxならnoneでも良いが色々制限があるし、マスタは独立させたいため。
    - 以下はUbuntu版

        ```
        sudo apt install libvirt-bin qemu-kvm
        sudo usermod -a -G libvirt $(whoami)
        newgrp libvirt

        curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 && chmod +x docker-machine-driver-kvm2 && sudo mv docker-machine-driver-kvm2 /usr/bin/
        ```

```
$ minikube start --vm-driver kvm2
$ kubectl run hello-minikube --image=k8s.gcr.io/echoserver:1.4 --port=8080
$ kubectl expose deployment hello-minikube --type=NodePort
$ kubectl get pod
NAME                            READY     STATUS    RESTARTS   AGE
hello-minikube-c6c6764d-bkpwx   1/1       Running   0          31s
$ curl $(minikube service hello-minikube --url)
$ kubectl delete service hello-minikube
$ kubectl delete deployment hello-minikube
$ minikube stop
```

```
$ minikube dashboard --url=true
http://192.168.39.215:30000
```

- `192.168.39.215`はminikubeの仮想マシンのアドレスで、`minikube`というホスト名指定でもいける。
- ダッシュボードの右上にある「+Create」からアプリを作成できる。
    - 設定（Yaml）のテキスト / ファイル入力での作成や個別のパラメタ設定での作成が可能
    - ポートの指定はクラスタ内／外に見せるかどうかと、クラスタ内用とコンテナ内用ポート

```
minikube delete
rm -rf .minikube/
```

- minikubeのdockerを使わせる
```
eval $(minikube docker-env)
```

- 元に戻す
```
eval $(minikube docker-env -u)
```

```
docker build -t hello-node:v1 .

kubectl run hello-node --image=hello-node:v1 --port=8080
$ kubectl get deployments
NAME         DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
hello-node   1         1         1            1           14s

$ kubectl get pods
NAME                         READY     STATUS    RESTARTS   AGE
hello-node-9f5f775d6-bxzbb   1/1       Running   0          28s

kubectl get events
kubectl config view
```

```
kubectl expose deployment hello-node --type=LoadBalancer
kubectl get services

$ minikube service hello-node
Opening kubernetes service default/hello-node in default browser...
$ No protocol specified
Unable to init server: Could not connect: 接続を拒否されました
Error: cannot open display: :1

$ minikube service hello-node --url=true
http://192.168.39.215:32709

$ kubectl logs hello-node-9f5f775d6-bxzbb
Received request for URL: /
Received request for URL: /favicon.ico
```

```
kubectl delete service hello-node
kubectl delete deployment hello-node

docker rmi hello-node:v1 -f
minikube stop
eval $(minikube docker-env -u)

minikube delete
```

### KVM
```
sudo virsh
list --all
shutdown minikube
undefine minikube
quit
```
