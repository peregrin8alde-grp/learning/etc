# CentOS7へのKubernetesインストール

練習としてCentos7ベースのコンテナにインストールしてみる。
＝＞　`--privileged`モードのせいでホストが操作不能になったので諦める。

https://kubernetes.io/docs/getting-started-guides/scratch/
https://exlair.net/installing-the-kubernetes-on-centos7/
https://qiita.com/Esfahan/items/db7a79816731e6aa5cf5
https://knowledge.sakura.ad.jp/3681/

- mycentosdev:7をベースにする。
- systemctl を多用するのでrootユーザで`--privileged`モード、`/sbin/init`起動
    - dockerデーモン起動とかにも必要
    - `--privileged --cap-add=SYS_ADMIN`のほうが範囲が狭い分安全？
    - どちらにせよ、ホストに影響を与えて操作不能になったので諦める。

## 共通
```
sudo yum -y install kubernetes flannel
```

## master
```
sudo yum -y install etcd

sudo vi /etc/etcd/etcd.conf
```

- 編集前

    ```
    ETCD_LISTEN_CLIENT_URLS="http://localhost:2379"
    ETCD_ADVERTISE_CLIENT_URLS="http://localhost:2379"
    ```

- 編集後

    ```
    ETCD_LISTEN_CLIENT_URLS="http://0.0.0.0:2379"
    ETCD_ADVERTISE_CLIENT_URLS="http://0.0.0.0:2379"
    ```

```
sudo systemctl start etcd
```
