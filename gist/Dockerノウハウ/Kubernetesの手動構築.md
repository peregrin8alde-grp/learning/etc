# Kubernetesの手動構築

Creating a Custom Cluster from Scratch
https://kubernetes.io/docs/getting-started-guides/scratch/

- 勉強という意味でこっちで実施
- かなり面倒なので、勉強以外では非推奨
- 参考：https://qiita.com/derui@github/items/09a1946b9de7f3ff3c1c


## Designing and Preparing
設計と前準備

### Downloading and Extracting Kubernetes Binaries
```
$ curl -L -O https://github.com/kubernetes/kubernetes/releases/download/v1.9.4/kubernetes.tar.gz
$ tar zxvf kubernetes.tar.gz
$ ./kubernetes/cluster/get-kube-binaries.sh
$ tar zxvf ./kubernetes/server/kubernetes-server-linux-amd64.tar.gz
$ ls -nh ./kubernetes/server/bin
合計 1.9G
-rwxr-xr-x 1 1000 1000  54M  3月 13 01:59 apiextensions-apiserver
-rwxr-xr-x 1 1000 1000 113M  3月 13 01:59 cloud-controller-manager
-rw-r--r-- 1 1000 1000    7  3月 13 01:59 cloud-controller-manager.docker_tag
-rw-r--r-- 1 1000 1000 114M  3月 13 01:59 cloud-controller-manager.tar
-rwxr-xr-x 1 1000 1000 241M  3月 13 01:59 hyperkube
-rwxr-xr-x 1 1000 1000  53M  3月 13 01:59 kube-aggregator
-rw-r--r-- 1 1000 1000    7  3月 13 01:59 kube-aggregator.docker_tag
-rw-r--r-- 1 1000 1000  54M  3月 13 01:59 kube-aggregator.tar
-rwxr-xr-x 1 1000 1000 201M  3月 13 01:59 kube-apiserver
-rw-r--r-- 1 1000 1000    7  3月 13 01:59 kube-apiserver.docker_tag
-rw-r--r-- 1 1000 1000 203M  3月 13 01:59 kube-apiserver.tar
-rwxr-xr-x 1 1000 1000 132M  3月 13 01:59 kube-controller-manager
-rw-r--r-- 1 1000 1000    7  3月 13 01:59 kube-controller-manager.docker_tag
-rw-r--r-- 1 1000 1000 133M  3月 13 01:59 kube-controller-manager.tar
-rwxr-xr-x 1 1000 1000  61M  3月 13 01:59 kube-proxy
-rw-r--r-- 1 1000 1000    7  3月 13 01:59 kube-proxy.docker_tag
-rw-r--r-- 1 1000 1000 107M  3月 13 01:59 kube-proxy.tar
-rwxr-xr-x 1 1000 1000  59M  3月 13 01:59 kube-scheduler
-rw-r--r-- 1 1000 1000    7  3月 13 01:59 kube-scheduler.docker_tag
-rw-r--r-- 1 1000 1000  61M  3月 13 01:59 kube-scheduler.tar
-rwxr-xr-x 1 1000 1000 146M  3月 13 01:59 kubeadm
-rwxr-xr-x 1 1000 1000  65M  3月 13 01:59 kubectl
-rwxr-xr-x 1 1000 1000 141M  3月 13 01:59 kubelet
-rwxr-xr-x 1 1000 1000 2.1M  3月 13 01:59 mounter
```

- Selecting Images
    - コンテナ外で動かすであろうもの（システムのデーモンとして動作）
        - docker
        - kubelet
        - kube-proxy
    - コンテナでの動作推奨
        - etcd
        - kube-apiserver
        - kube-controller-manager
        - kube-scheduler
    - コンテナで動作するものをどうビルドするかの話
        - 「Google Container Registry (GCR)」に登録されているものを使う。
            - `gcr.io/google-containers/hyperkube:$TAG`
            - `hyperkube`というall in oneバイナリを使えば`hyperkube kubelet`といった感じで実行できる。
        - 自分でビルドする。
            - `./kubernetes/server/bin/kube-apiserver.tar`が使える。

                ```
                docker load -i ./kubernetes/server/bin/kube-apiserver.tar
                docker load -i ./kubernetes/server/bin/kube-controller-manager.tar
                docker load -i ./kubernetes/server/bin/kube-scheduler.tar
                docker images
                〜
                gcr.io/google_containers/kube-controller-manager   v1.9.4              35c62345e5ac        5 days ago          139MB
                gcr.io/google_containers/kube-scheduler            v1.9.4              897eabbc86ac        5 days ago          62.9MB
                gcr.io/google_containers/kube-apiserver            v1.9.4              3945a0b35e33        5 days ago          212MB
                ```
        - Etcd
            - 「Google Container Registry (GCR)」に登録されているものを使う。  
              `gcr.io/google-containers/etcd:2.2.1`
            - 「Docker Hub」か「Quay.io」に登録されているものを使う。  
              `quay.io/coreos/etcd:v2.2.1`
            - OSに入っているものを使う。
            - 自分でイメージをビルド  
              `cd kubernetes/cluster/images/etcd; make`
            - `Kubernetes binary`のバージョンを推奨  
              `kubernetes/cluster/images/etcd/Makefile`のTAG変数で確認できる。

                ```
                TAGS?=2.2.1 2.3.7 3.0.17 3.1.11
                REGISTRY_TAG?=3.1.11
                ```
            - 面倒なのでGCRを利用

                ```
                docker pull gcr.io/google-containers/etcd:3.1.11
                ```

### Preparing Certs
- マスターをHTTPSとする場合にCertが必要
- ノード（kubelet）側はオプション
- 実CAは使っていないので自己証明書を用意する。
  https://kubernetes.io/docs/admin/authentication/#creating-certificates/
- まずは使わないでHTTPから

## Configuring and Installing Base Software on Nodes
各ノードでのインストール作業

- docker or rkt
- kubelet
- kube-proxy

### docker
- Dockerをインストール
- 自動で生成されるネットワークは邪魔になるので消す？

### kubelet
- ダウンロード／展開したバイナリを使う。
./kubernetes/server/bin/kubelet

- `--config=/etc/kubernetes/manifests`  
  kubeletが監視するディレクトリ。この配下にmanifestsファイルを格納する。  
  存在しない場合は新規作成でいい？

    ```
    sudo mkdir -p /etc/kubernetes/manifests
    ```

    - `--config`オプションは存在しない？
- `--cluster-dns=`  
  DNSサーバのアドレス
    - 自分で用意？  
      https://kubernetes.io/docs/getting-started-guides/scratch/#starting-cluster-services
- `--cluster-domain=`  
  DNSドメインのprefix。
- `--docker-root=`
- `--root-dir=`
- `--pod-cidr=`  
  The CIDR to use for pod IP addresses, only used in standalone mode.  
  In cluster mode, this is obtained from the master
- `--register-node`
    - 参照：https://kubernetes.io/docs/concepts/architecture/nodes/

- 実行しやすくするために以下を実行  
  `sudo cp -p ./kubernetes/server/bin/kubelet /usr/local/bin/`
- オプションは以下も参照  
  https://kubernetes.io/docs/reference/generated/kubelet/

```
sudo kubelet --config=/etc/kubernetes/manifests
```

### kube-proxy
- `--master=http://$MASTER_IP`
- 実行しやすくするために以下を実行  
  `sudo cp -p ./kubernetes/server/bin/kube-proxy /usr/local/bin/`

### Networking

## Bootstrapping the Cluster
クラスタの作成

### etcd
1. Copy cluster/gce/manifests/etcd.manifest
  https://github.com/kubernetes/kubernetes/blob/master/cluster/gce/manifests/etcd.manifest
2. 必要に応じて修正
    - リポジトリ名とバージョンだけ変えてみた。
3. Start the pod by putting it into the kubelet manifest directory

    ```
    sudo cp etcd.manifest /etc/kubernetes/manifests/
    ```

kubeletの`--config`オプション時点でエラーとなったので、一旦ここで断念
