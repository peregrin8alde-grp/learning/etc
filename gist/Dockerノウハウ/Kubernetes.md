# Kubernetes

コンテナオーケストラツール

https://kubernetes.io/
https://github.com/kubernetes/kubernetes

https://ja.wikipedia.org/wiki/Kubernetes

https://kubernetes.io/docs/setup/


- マスター／スレーブ（ノード）型
    - マスター：ノード管理のためのプロセス群
    - ノード：サービスが動作するホスト
- kubectlによるAPI呼び出しがメインでminikubeやkubeadmはあくまで入門向け管理用ツール
- 本格的に使うなら各種コンポーネントを含めて自分で入れるか、ツールを利用
    - 参考：https://knowledge.sakura.ad.jp/3681/
    - etcd：分散型設定共有サービス（KVS）。各コンテナのIPを管理するためなどに使う。必須。
    - flannel：複数マシンでのDockerコンテナ間の通信を簡易化するための仮想ネットワークを構築する。
      各コンテナに個別のIPを割り当てる。複数ノード時に使う。
