# Ubuntu上でのKubernetesインストール
## Kubernetes on Ubuntu
https://kubernetes.io/docs/getting-started-guides/ubuntu/
https://www.ubuntu.com/kubernetes
https://tutorials.ubuntu.com/tutorial/install-kubernetes-with-conjure-up?backURL=%2F&_ga=2.245506308.1643330402.1521299813-2102886945.1521299813#0

```
sudo snap install conjure-up --classic
conjure-up
```

1. "kubernetes" - "The Canonical Distribution of Kubernetes"
    - 小規模やお試し用に"kubernetes Core"というのがある。
2. Rancher 2.0などが選択肢にあるので好きなものを選んで進む。
    - LXDがないと怒られる。
      `sudo snap install lxd`
