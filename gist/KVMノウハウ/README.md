# KVMノウハウ

Ubuntu上で実施

## install
参考：https://www.hiroom2.com/2017/09/03/ubuntu-1704-kvm-ja/

1. qemu-kvmとlibvirtのインストール
    - qemu-kvmとKVM設定用のパッケージ
    - libvirt-binをsystemdに登録
        - 既に登録済み？でエラーが出たので省略

    ```
    sudo apt install -y qemu-kvm libvirt0 libvirt-bin virt-manager
    ```

2. bridgeインターフェース  
  外部から仮想マシンへアクセスする場合は、bridgeインターフェースを設定。詳細は次節参照。
    - Ubuntuが起動しなくなった。`/etc/network/interfaces`を削除したら起動したため、  
      これのせいの可能性が高い。
        - `bridge_ports`で指定した名前が存在しなかったから？
            - 正解。`ip addr`でホストが外との通信に使っているものを確認して正しい値を記述する。

    ```
    sudo apt install -y bridge-utils
    cat <<EOF | sudo tee /etc/network/interfaces
    auto lo
    iface lo inet loopback
    auto br0
    iface br0 inet dhcp
        bridge_ports ens3
        bridge_stp off
        bridge_maxwait 0
    EOF
    sudo reboot
    ```

3. libvirtグループ  
  libvirtグループに所属しているとsudoなしでvirt-managerやvirshを実行可能

    ```
    $ sudo gpasswd libvirt -a $(whoami)
    $ sudo reboot
    ```

## ネットワーク

ゲストが外に通信するためのブリッジbr0を作成しておく。

https://www.hiroom2.com/2017/11/30/ubuntu-1710-bridge-ja/

1. bridge-utilsのインストール

    ```
    sudo apt install -y bridge-utils
    ```

2. bridge作成前の状態

    ```
    $ ip addr show
    〜略〜
    2: enp3s0:〜略〜
    〜略〜
    ```

    ```
    $ brctl show
    bridge name	bridge id		STP enabled	interfaces
    br0		8000.1831bf4facf9	no		enp3s0	
    virbr0		8000.525400fc67da	yes		virbr0-nic
    ```

3. bridgeの作成  
  ens3の設定をbr0へ移動

    1. DHCPを用いる場合

        ```
        cat <<EOF | sudo tee /etc/network/interfaces
        auto lo
        iface lo inet loopback
        auto br0
        iface br0 inet dhcp
            bridge_ports enp3s0
            bridge_stp off
            bridge_maxwait 0
        EOF
        sudo reboot
        ```

4. bridge作成後の状態  
  enp3s0と同じMACアドレスを持つbr0が作成され、br0にIPアドレスが割り当てられる。

    ```
    $ ip addr show
    〜略〜
    2: enp3s0:〜略〜
    3: br0:〜略〜
    〜略〜
    ```

    ```
    $ brctl show
    bridge name	bridge id		STP enabled	interfaces
    br0		8000.1831bf4facf9	no		enp3s0	
    virbr0		8000.525400fc67da	yes		virbr0-nic
    ```

## virt-install を使用したゲストの作成
https://access.redhat.com/documentation/ja-jp/red_hat_enterprise_linux/6/html/virtualization_host_configuration_and_guest_installation_guide/sect-virtualization_host_configuration_and_guest_installation_guide-guest_installation-creating_guests_with_virt_install

https://www.fuketch.net/centos-7-kvm%E4%BB%AE%E6%83%B3%E5%8C%96%E3%82%B2%E3%82%B9%E3%83%88%E3%81%AE%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB/

1. isoの入手
2. kickstart設定ファイルを作成
    - `centos7.ks.cfg`参照。必要に応じてユーザ名やパスワードなどを変更。gatewayなども注意。
3. ゲストOS用ディスクイメージの作成

    ```
    $ sudo qemu-img create -f qcow2 /var/lib/libvirt/images/disk01.qcow2 32G
    Formatting '/var/lib/libvirt/images/disk01.qcow2', fmt=qcow2 size=34359738368 cluster_size=65536 lazy_refcounts=off refcount_bits=16
    ```

4. インストール

    ```
    $ sudo virt-install \
    --name centos7 \
    --hvm \
    --virt-type kvm \
    --ram 2048 \
    --vcpus 2 \
    --arch x86_64 \
    --os-type linux \
    --os-variant rhel7 \
    --boot hd \
    --disk path=/var/lib/libvirt/images/disk01.qcow2 \
    --network bridge=br0,model=virtio \
    --graphics none \
    --serial pty \
    --console pty \
    --location [isoの存在するディレクトリ]/CentOS-7-x86_64-Minimal-1611.iso \
    --initrd-inject [kickstart設定ファイルの存在するディレクトリ]/centos7.ks.cfg \
    --extra-args "inst.ks=file:/centos7.ks.cfg console=ttyS0"
    ```

5. コンソールを抜ける  
  `Ctrl+]`
6. 状態確認  

    ```
    $ virsh list --all
    Id    名前                         状態
    ----------------------------------------------------
    2     centos7                        実行中
    ```

## その他
- VM起動  
  `virsh start centos7`
- VM停止  
  `virsh shutdown centos7`
- コンソールに入る  
  `virsh console centos7`
- VM削除  
  `virsh undefine centos7`
- VM一覧  
  `virsh list --all`



