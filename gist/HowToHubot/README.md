# Hubot

https://hubot.github.com/

## install
### CentOS7
1. 必要なパッケージをインストール。

    ```
    yum -y update \
    && yum -y install epel-release \
    && yum -y install nodejs npm
    ```

2. gitでの管理を想定した作りのため、gitもインストールしておく。

    ```
    yum -y install git
    ```

3. 必要に応じて日本語設定などを実行。

    ```
    localedef -i ja_JP -f UTF-8 ja_JP.UTF-8 \
    && localedef -i ja_JP -f EUC-JP ja_JP.eucJP \
    && localedef -i ja_JP -f SHIFT_JIS ja_JP.sjis \
    && echo LANG="ja_JP.UTF-8" > /etc/locale.conf \
    && export LANG="ja_JP.UTF-8" \
    && unlink /etc/localtime \
    && ln -s /usr/share/zoneinfo/Japan /etc/localtime
    ```

4. Hubot生成ツールのインストール実行

    ```
    npm install -g yo generator-hubot
    ```

## setup
1. rootだと`yo hubot`で権限エラーになるため、専用のユーザを用意

    ```
    useradd -m -d /home/hubot -s /bin/bash hubot \
    && echo "hubot:hubot" | chpasswd \
    && su hubot \
    && cd /home/hubot
    ```

2. プロジェクト初期化
    - 各種オプションについては以下を参照。
      https://hubot.github.com/docs/

    ```
    mkdir myhubot
    cd myhubot
    yo hubot
    ```

## 実行
- 基本

    ```
    bin/hubot
    ```

- アダプタやボット名指定

    ```
    bin/hubot -a アダプタ -n ボット名
    ```

- `HUBOT_HEROKU_KEEPALIVE_URL`に関するエラーは無視
    - herokuは使わないため。
- ボット名でチャットに入ったような感じになる。
  このチャットに送られた発言にbotが反応することになる。
- 発言の先頭にmyhubotなどのボット名をつけると、respond向けの発言として
  反応される。
    - ボット名は大文字小文字を区別しない。
- `bin/hubot`時に`--adapter`を指定しないと、`yo hubot`時に指定していても使われない？
    - 指定なしだと`Shell`に繋げる。
      `ボット名> メッセージ`という感じでCUI上でチャットを行う。
    - slack指定で起動するとslackに繋げにいく。
        - 接続回りの設定は環境変数で行う。他のチャットツールでも同様。
- 起動し続けるには一工夫必要  
  参照：https://hubot.github.com/docs/deploying/unix/  
  簡単なのはnohupでの実行。
    - 起動：`nohup bin/hubot >nohup_out.log 2>nohup_err.log &`
      この後`exit`すると終了してしまうので、`logout`する。dockerの場合は`Ctrl+p, Ctrl+q`。
    - 停止：`ps`で調べて`kill`。
- メッセージ例（SHELLモード）

    ```
    myhubot help
    myhubot ping
    exit
    ```

## Dockerfile
- Dockerfile編集、内容は別資料のDockerfile参照
    - `yo hubot`以降を自由に変更する使い方を想定

    ```
    mkdir -p ~/hubot/docker
    vi ~/hubot/docker/Dockerfile
    ```

- build

    ```
    docker build -t myhubot:latest \
                ~/hubot/docker
    ```

- run
    - 作業用ディレクトリは前もって作成
        - 自動生成だとroot権限で作成されてアクセスできなかったため。
    - 初期化や起動時のオプションを自由とするため、デフォルトは`/bin/bash`起動
    - webの待ち受けには8080が使われる。

    ```
    mkdir -p ~/hubot/work/hubot01

    docker run -it \
            --name hubot \
            -p 38080:8080 \
            -v ~/hubot/work/hubot01:/home/hubot/hubot01 \
            -w /home/hubot/hubot01 \
            myhubot
    ```

## script
https://hubot.github.com/docs/scripting/

- 自作のものは`scripts`配下におけば自動で読み込まれる。

