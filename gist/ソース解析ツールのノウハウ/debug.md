# デバッグ

```
docker run \
  -it \
  --name trygdbsrv \
  -p 1234:1234 \
  --privileged \
  centos:7 \
    /bin/bash
```

```
yum update -y \
&& yum install -y gdb gdb-gdbserver

gdbserver --multi :1234
```


```
## gdb内でrun実行時にptrace権限が必要
docker run -it \
  --name trygdbcli \
  --cap-add=SYS_PTRACE --security-opt="seccomp=unconfined" \
  -p 25000:5000 \
  -v $(pwd):/work \
  -w /work \
  ubuntu \
    /bin/bash
```

```
apt update \
&& apt install -y gdb
apt install -y wget
wget https://gdbgui.com/downloads/linux/gdbgui_0.13.0.0
```

```
cat .gdbinit
target extended-remote trygdbsrv:1234
```
