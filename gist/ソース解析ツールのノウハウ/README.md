# ソース解析ツールのノウハウ

静的解析ツールのノウハウ
特にC系

## ビルド時のオプションを利用するパターン
- gccなどのコマンドをPATHの優先度を利用して置き換えてしまうのが楽そう。

```
## 自分のホームディレクトリにbinディレクトリを作成
$ mkdir ~/bin

## サーチパスの先頭におく
$ echo 'export PATH=${HOME}/bin:${PATH}' >> .bashrc

## ラッパースクリプトをコピー
$ cp rtags/bin/gcc-rtags-wrapper.sh ~/bin/gcc
$ cp rtags/bin/gcc-rtags-wrapper.sh ~/bin/g++
```

- makeを使う場合は[Bear](https://github.com/rizsotto/Bear)というツールがあるらしい。
  clang使うツールで`compile_commands.json`作成したい場合向け。
- clang >= 5.0では`-MJ`付きでコンパイルすると`compile_commands.json`を作成してくれる？
  参考：https://github.com/cquery-project/cquery/wiki/Compilation-database
