```
- hosts: all
  remote_user: user01
  become: yes
  tasks:
    - name: upgrade all packages
      yum:
        name: '*'
        state: latest
    - name: docker packages installed
      yum:
        name: "{{ packages }}"
      vars:
        packages:
        - yum-utils
        - device-mapper-persistent-data
        - lvm2
    - name: add docker repository
      command: yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    - name: docker installed
      yum:
        name: docker-ce
        state: latest
    - name: make sure docker is running
      service:
        name: docker
        state: started
    - name: docker hello-world
      command: docker run hello-world
    - name: Add the user for docker group
      user:
        name: user01
        groups: docker
        append: yes
```