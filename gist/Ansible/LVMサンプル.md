- https://docs.ansible.com/ansible/latest/modules/lvg_module.html#lvg-module
- https://docs.ansible.com/ansible/latest/modules/lvol_module.html#lvol-module
- 実行した時点でマウントやfstabの編集が完了する。

```
- hosts: all
  remote_user: user01
  become: yes
  tasks:
    - name: test
      ping:
    - name: fdisk
      command: fdisk -l
    - name: Create a volume group
      lvg:
        vg: vg.services
        pvs: /dev/sdb
    - name: Create a logical volume
      lvol:
        vg: vg.services
        lv: test
        size: 5g
    - name: Create a logical volume2
      lvol:
        vg: vg.services
        lv: test2
        size: 5g
    - name: lvdisplay
      command: lvdisplay
    - name: Create a filesystem
      filesystem:
        fstype: xfs
        dev: /dev/vg.services/test
        #resizefs: yes
    - name: Create a filesystem2
      filesystem:
        fstype: xfs
        dev: /dev/vg.services/test2
        #resizefs: yes
    - name: Mount up device
      mount:
        path: /mnt/test
        src: /dev/vg.services/test
        fstype: xfs
        state: mounted
    - name: Mount up device2
      mount:
        path: /mnt/test2
        src: /dev/vg.services/test2
        fstype: xfs
        state: mounted
```