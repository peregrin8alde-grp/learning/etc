https://docs.ansible.com/

## install
https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

```
sudo pip install ansible
```

## Getting Started
https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html

`/etc/ansible/hosts`

```
192.0.2.50
aserver.example.org
bserver.example.org
```

- 前もってSSH公開鍵を登録、接続確認しておくこと
- ssh-agentを使うことで自動で秘密鍵を使う。

```
$ ssh-agent bash
$ ssh-add ~/.ssh/id_rsa
```

```
# With latest version of ansible `sudo` is deprecated so use become
# as bruce, sudoing to root
$ ansible all -m ping -u bruce -b
# as bruce, sudoing to batman
$ ansible all -m ping -u bruce -b --become-user batman

$ ansible all -a "/bin/echo hello"
```

## playbook

```
ansible-playbook -v tmpplaybook.yml
```
