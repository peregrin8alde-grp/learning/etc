# OpenShiftコミュニティ版

https://www.okd.io/

## Local Cluster Management
https://github.com/openshift/origin/blob/v4.0.0-alpha.0/docs/cluster_up_down.md

- /etc/docker/daemon.json
- ここで追加したサーバがレジストリに使われるため、プロキシ配下ではDockerのNO_PROXYに追加する必要がある。
    - cluster up時に警告が出る。

```
{
   "insecure-registries": [
     "172.30.0.0/16"
   ]
}
```

```
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```

```
$ docker network inspect -f "{{range .IPAM.Config }}{{ .Subnet }}{{end}}" bridge

sudo firewall-cmd --permanent --new-zone dockerc
sudo firewall-cmd --permanent --zone dockerc --add-source 172.17.0.0/16
sudo firewall-cmd --permanent --zone dockerc --add-port 8443/tcp
sudo firewall-cmd --permanent --zone dockerc --add-port 53/udp
sudo firewall-cmd --permanent --zone dockerc --add-port 8053/udp
sudo firewall-cmd --reload
```

```
curl -LO https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
tar zxvf openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
sudo cp openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit/oc /usr/local/bin/
```

```
oc cluster up
```

```
The server is accessible via web console at:
    https://127.0.0.1:8443

You are logged in as:
    User:     developer
    Password: <any value>

To login as administrator:
    oc login -u system:admin
```

```
oc cluster down
```

- 外部から接続
    - `127.0.0.1`にリダイレクトされてしまい上手くいかない
        - https://github.com/openshift/origin/issues/19699
        - no_proxyの設定が必要？無駄？
          ```
          mkdir ~/.docker
          vi ~/.docker/config.json

          {
              "proxies":
              {
                  "default":
                  {
                      "noProxy": "localhost,127.0.0.1,centos7-1,192.168.56.101"
                  }
              }
          }
          ```
        - `oc cluster up`時のオプションで`--no-proxy`を指定したら上手くいった。
            - 同じ指定や環境のつもりでも上手くいかない場合があるのでまだ原因は不明
            - `~/.docker/config.json`の設定で変化する可能性あり
            - ブラウザ側など他の場所に情報が残っているのか、no_proxyをjson/コマンドともに消しても上手くいく場合がある。
            - クラスタ内でのプロキシ設定の可能性もあるため、マスターとなるマシンのIPかホスト名、ドメインなどを指定？
        - `.kube`や`openshift.local.clusterup`も削除しないと古い情報が残る可能性あり
            - cluster downしても`openshift.local.clusterup`が使用中で削除できない？作成したサービスは起動しっぱなしの模様。
        - `--no-proxy=[$no_proxy]`のように環境変数指定は不可？
    - `--routing-suffix=nip.io`のように指定をroutingを指定しないと`127.0.0.1.nip.io`が各サービスのURLに付加される
        - `openshift.local.clusterup`もちゃんと消さないと上手くいかない？
        - 正しいroutingは別途確認
            - `nip.io`は[ワイルドカードDNSサービス](http://nip.io/)であり、IPアドレスをドメイン名に含めるとそのIPアドレスを返す。
            - 各サービスのドメイン名が`[サービス名]-[プロジェクト名].[suffix]`となるので、このドメイン名をクラスタ内のドメインを管理してるDNSで解決するように設定？
                - クラスタ内PODは自動でやってくれそうだが、接続元は自分でDNSにワイルドカード指定などで追加？
            - 参考：
                - http://n-agetsuma.hatenablog.com/entry/2018/06/10/220852
                - https://qiita.com/kimura-y/items/869f8e72a09ffd36e816

```
sudo firewall-cmd --permanent --add-port 8443/tcp
sudo firewall-cmd --reload

oc cluster up \
  --public-hostname=centos7-1.local.com \
  --no-proxy=[localhost,127.0.0.1,.local.com] \
  --routing-suffix=nip.io
```

```
OpenShift server started.

The server is accessible via web console at:
    https://centos7-1.local.com:8443
```

```
oc cluster up \
  --public-hostname=192.168.56.101 \
  --no-proxy=[localhost,127.0.0.1,192.168.56.101] \
  --routing-suffix=192.168.56.101.nip.io
```
