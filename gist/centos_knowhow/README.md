# CentOS7でのntfsフォーマットHDDマウント
CentOS7でWindowsで使っていたHDDを認識させる。
詳細までは未確認。

```
# yum install epel-release
# yum -y --enablerepo=epel install dkms fuse-ntfs-3g
```

# DVD書き込み
```
dvdrecord isofilename.iso
```

# ネットワーク設定変更
```
nmcli -p connection show eth0

nmcli connection modify eth0 ipv4.gateway 192.168.3.1
nmcli connection modify eth0 ipv4.dns 192.168.3.1
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.3.100/24

systemctl restart network.service
```
